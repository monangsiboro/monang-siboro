package com.example.recyclerproject.control;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerproject.R;
import com.example.recyclerproject.model.IdentitasModel;

import java.util.List;

public class IdentitasAdapter extends RecyclerView.Adapter<IdentitasAdapter.ViewHolder> {

    private Context context;
    private List<IdentitasModel> list;

    public IdentitasAdapter(Context context, List<IdentitasModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.model_data_nama, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        IdentitasModel identitas = list.get(position);

        holder.textTitle.setText(identitas.getTitle());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textTitle;

        public ViewHolder(View itemView) {
            super(itemView);

            textTitle = itemView.findViewById(R.id.nama);
        }
    }

}